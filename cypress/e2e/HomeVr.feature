Feature: Validar a página Home do site da Vr
    Como um usuário do site VR
    Eu quero validar acões na página Home
    Para que eu possa validar as interacões da página

    Scenario: Validar botão de acessar a loja da VR
        Given que eu acesso a home do portal web
        When eu clicar no botão "Compre online"
        Then valido que o botão tenta abrir uma nova aba ou janela

