import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

import HomePage from '../../support/pages/HomePage'

const homePage = new HomePage();

Given(/^que eu acesso a home do portal web$/, () => {
    cy.visit("/");
    homePage.validaUrl();
});

When(/^eu clicar no botão "([^"]*)"$/, (nomeBtn) => {
    homePage.acessarLojaOnline(nomeBtn);
});


Then(/^valido que o botão tenta abrir uma nova aba ou janela$/, () => {
    homePage.validaSeBotaoPodeInteragir();
});