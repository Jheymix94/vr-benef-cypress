import HomeElements from '../elements/HomeElements';

class HomePage {
    
    homeEl = new HomeElements();

    /**
     * Clica no botão para acessar a loja online.
     * @param {string} nomeBtn - O texto do botão que será clicado.
     */
    acessarLojaOnline = (nomeBtn) => {
        // Configura um stub para a função window.open
        cy.window().then((win) => {
            cy.stub(win, 'open').as('windowOpen');
        });

        // Clica no botão de compra online que contém o nome especificado no BDD
        cy.get(this.homeEl.btnCompraOnline()).contains(nomeBtn).click();
    }

    /**
     * Valida se o botão clicado tentou abrir uma nova janela ou aba.
     */
    validaSeBotaoPodeInteragir = () => {
        // Verifica se a função window.open foi chamada
        cy.get('@windowOpen').should('have.been.called');
    }

    /**
     * Verifica se a URL atual é a URL da home do site VR.
     */
    validaUrl = () => {
        cy.url().should('eq', Cypress.env('homeUrlSiteVr'));
    }

    /**
     * Verifica se o título da página é o esperado para a home do site VR.
     */
    validaTituloPaginaHome = () => {
        cy.title().should('eq', Cypress.env('homeTitle'));
    }
}

export default HomePage;
