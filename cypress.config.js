const cucumber = require('cypress-cucumber-preprocessor').default

module.exports = {
  e2e: {
    video: false,
    viewportHeight: 800,
    viewportWidth: 1400,
    pageLoadTimeout: 30000,
    responseTimeout: 30000,
    defaultCommandTimeout: 40000,
    clearLocalStorage: true,
    clearSessionStorage: true,
    supportFile: 'cypress/support/e2e.js',
    baseUrl: "https://www.vr.com.br",
    specPattern: 'cypress/e2e/**/*.feature',
    env: {
      homeUrlSiteVr: 'https://www.vr.com.br/',
      homeUrlSiteLojaOnlineVr: 'https://loja.vr.com.br/',
      homeTitle: 'VR - Refeição, Transporte, Controle de Ponto e Soluçoes Financeiras',
      tituloModalLoja: 'Bem-vindo(a) à Loja VR!',
      subTituloModalLoja: 'Como podemos te ajudar?',
      opcaoVrParaMeusFuncionarios: 'Quero oferecer os serviços da VR para meus funcionários',
      opcaoTenhoEstabelecimentoComercial: 'Tenho um Estabelecimento Comercial',
      opcaoTenhoCartaoVr: 'Tenho um cartão de benefícios da VR',
      textoProdutoAdicionado: 'Produto adicionado!',
    },

    setupNodeEvents(on, config) {
      on('file:preprocessor', cucumber());
      return config;
    },
    
  },
};