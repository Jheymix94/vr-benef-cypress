class AdicionarProdutoElements {
    // Botoes
    btnOpcaoVrParaMeusFuncionarios = () => 'link-cliente-rh';
    btnOpcaoTenhoEstabelecimentoComercial = () => 'link-cliente-ec';
    btnOpcaoTenhoCartaoVr = () => 'link-cliente-trabalhador';

    btnFormularios1 = () => 'article[data-testid^="shelf-product-container"]';
    btnFormularios2 = () => '.shelf-product-container__button';
    btnAceitaCookies = () => '[data-testid="terms-consent"]';
    btnPlusButton = () => '.plus-button';
    btnAdicionarCarrinho = () => '[id^="btn-adicionar-carrinho"]';

    //Validacoes
    campoTituloSolucoes = () => '.shelf-product-container__title span';
    campoTituloCartaoVr = () => '.individual-product-header-container__title-container h2 span';
    
    msgProdutoadicionado = () => 'div.product-in-cart-view__content p';
}

export default AdicionarProdutoElements;
