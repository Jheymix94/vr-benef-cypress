# Projeto de Automação com Cypress para o Site VR

## Descrição
Este projeto utiliza o Cypress para automatizar testes no site da VR, validando a página inicial e o processo de compra de cartões VR.

## Instalação

1. **Clone o repositório:**
```
- git clone https://gitlab.com/Jheymix94/vr-benef-cypress.git
- cd projeto-cypress-vr
```

2. **Instale as dependências:**
```
- npm install
```

## Uso

### Executar os Testes
Para executar os testes de automação, use o comando:
```
- npm run testAndReport: Executará todos os testes e gerará um relatório HTML usando `cucumber-html-reporter`.
- npm run start: Ira abrir o modo interface do Cypress
```

## Estrutura do Projeto
```plaintext
vr-benef-cypress/
├── cypress/
│   ├── e2e/
│   │   ├── HomeVr
│   │   │   ├── HomeVrSteps.js
│   │   ├── AdicionarProduto
│   │   │   ├── AdicionarProdutoSteps.js
│   │   ├── HomeVr.feature
│   │   ├── AdicionarProduto.feature
│   ├── fixtures/
│   ├── support/
│   │   ├── elements
│   │   │   ├── AdicionarProdutoElements.js
│   │   │   ├── HomeElements.js
│   │   ├── pages
│   │   │   ├── adicionarProdutoPage.js
│   │   │   ├── HomePage.js
│   │   ├── commands.js
│   │   ├── e2e.js
├── cypress.config.js
├── .gitignore.js
├── .gitlab-ci.yml.js
├── Dockerfile
├── package.json
├── cucumber-html-reporter.js
└── README.md


## Configurações de Exceção no Cypress
```
No arquivo `cypress/support/e2e.js`, configurei o Cypress para lidar com exceções não capturadas de maneira específica. Isso é feito para evitar que erros conhecidos e esperados interrompam a execução dos testes.
```