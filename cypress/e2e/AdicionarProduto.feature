Feature: Compra de Cartões VR
  Como um usuário do site VR
  Eu quero comprar cartões de forma online
  Para que eu possa gerenciar minhas compras diretamente pelo portal

  Scenario: Comprar cartões VR do tipo Auto
    Given que eu acesso a loja da VR
    And selecionar a opcão da modal "Quero oferecer os serviços da VR para meus funcionários"
    When selecionar a opção "Cartões VR"
    And adicionar uma quantidade aleatória de cartões do produto "Auto"
    And digitar um valor de crédito aleatório para o produto "Auto"
    And clicar no botão "Adicionar ao carrinho"
    Then o produto deve ser adicionado ao carrinho com sucesso
