import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

import AdicionarProdutoPage from '../../support/pages/adicionarProdutoPage'

const adicionarProdutoPage = new AdicionarProdutoPage();

Given(/^que eu acesso a loja da VR$/, () => {
  cy.visit(Cypress.env('homeUrlSiteLojaOnlineVr'));
  adicionarProdutoPage.aceitarCookies();
  adicionarProdutoPage.validaUrlLojaOnline();
});

When(/^selecionar a opcão da modal "([^"]*)"$/, (descricaoOpcao) => {
  adicionarProdutoPage.selecionarOpcaoModal(descricaoOpcao);
});

When(/^selecionar a opção "([^"]*)"$/, (opcao) => {
  adicionarProdutoPage.selecionarOpcaoSolucoes(opcao);
});

When(/^adicionar uma quantidade aleatória de cartões do produto "([^"]*)"$/, (nomeProduto) => {
    adicionarProdutoPage.selecionarCartao(nomeProduto);
});

When(/^digitar um valor de crédito aleatório para o produto "([^"]*)"$/, (produto) => {
  adicionarProdutoPage.digitarValorAleatorio(produto);
});

When(/^clicar no botão "Adicionar ao carrinho"$/, () => {
  adicionarProdutoPage.adicionarAoCarrinho();
});

Then(/^o produto deve ser adicionado ao carrinho com sucesso$/, () => {
  adicionarProdutoPage.validarProdutoAdicionado();
});
