import AdicionarProdutoElements from '../elements/AdicionarProdutoElements'

class AdicionarProdutoPage {
    
    AdicionarProdutoEl = new AdicionarProdutoElements();

    formularioProduto = null;

    /**
     * Seleciona a opção na modal que tem as opcões de como a VR pode me ajudar.
     * @param {string} descricaoOpcao.
     */
    selecionarOpcaoModal = (descricaoOpcao) => {
        const opcoes = [
            { testId: this.AdicionarProdutoEl.btnOpcaoVrParaMeusFuncionarios(), description: Cypress.env('opcaoVrParaMeusFuncionarios') },
            { testId: this.AdicionarProdutoEl.btnOpcaoTenhoEstabelecimentoComercial(), description: Cypress.env('opcaoTenhoEstabelecimentoComercial') },
            { testId: this.AdicionarProdutoEl.btnOpcaoTenhoCartaoVr(), description: Cypress.env('opcaoTenhoCartaoVr') }
        ];
        
        opcoes.forEach(opcao => {
            if (opcao.description.includes(descricaoOpcao)) {
                cy.get(`[data-testid="${opcao.testId}"] p`)
                    .should('contain.text', opcao.description)
                    .parents('[data-testid]')
                    .click();
            }
        });
    }
    
     /**
     * Seleciona uma opção na seção de soluções.
     * @param {string} descricaoOpcao
     */
    selecionarOpcaoSolucoes = (descricaoOpcao) => {
        cy.get(this.AdicionarProdutoEl.btnFormularios1()).each(($element) => {
            const title = $element.find(this.AdicionarProdutoEl.campoTituloSolucoes()).text();
            if (title.includes(descricaoOpcao)) {
                cy.wrap($element).find(this.AdicionarProdutoEl.btnFormularios2()).click();
            }
        });
    }

     /**
     * Seleciona um cartão específico baseado no nome do cartão fornecido.
     * Realiza um número aleatório de cliques no botão de adicionar quantidade.
     * @param {string} nomeCartao
     */
    selecionarCartao = (nomeCartao) => {
        cy.contains(this.AdicionarProdutoEl.campoTituloCartaoVr(), nomeCartao)
          .parents('form')
          .as('FormularioProduto');
        
        const cliquesAleatorios = Math.floor(Math.random() * 10) + 1;
        cy.get('@FormularioProduto')
          .find(this.AdicionarProdutoEl.btnPlusButton())
          .as('BotaoMais');
        
        for (let i = 0; i < cliquesAleatorios; i++) {
            cy.get('@BotaoMais').click();
        }
    }  

     /**
     * Gera um valor aleatório e insere nos campos de texto e número do formulário selecionado.
     */
    digitarValorAleatorio = () => {
        const valorAleatorio = (Math.random() * 100).toFixed(2);
    
        cy.get('@FormularioProduto')
            .find('input[type="number"]')  
            .invoke('val')
            .then(quantidade => {
                this.quantidade = quantidade;

                cy.get('@FormularioProduto')
                    .find('[type="text"]') 
                    .first()
                    .clear()
                    .type(valorAleatorio);
    
                cy.get('@FormularioProduto')
                    .find('input[type="number"]')  
                    .clear()
                    .type(this.quantidade);
            });
    }
    
     /**
     * Clica no botão de adicionar ao carrinho da ocpcão do cartão.
     */
    adicionarAoCarrinho = () => {
        cy.get('@FormularioProduto')
          .find(this.AdicionarProdutoEl.btnAdicionarCarrinho())
          .click();
    }

    /**
     * Aceita os cookies clicando no botão correspondente.
     * Pensei em colocar em outro arquivo, pois isso é do site e não da funcionalidade em sí, mas devido ao tempo, deixei aqui.
     */
    aceitarCookies = () => {
        cy.get(this.AdicionarProdutoEl.btnAceitaCookies())
           .should('be.visible')
           .click({ force: true });
    }

     /**
     * Verifica se a URL atual corresponde à URL da loja online especificada nas variáveis de ambiente.
     */
    validaUrlLojaOnline = () => {
        cy.url()
          .should('eq', Cypress.env('homeUrlSiteLojaOnlineVr'));
    }

     /**
     * Valida que uma mensagem de produto adicionado.
     */
    validarProdutoAdicionado = () => {
        cy.get(this.AdicionarProdutoEl.msgProdutoadicionado()).should('contain.text', Cypress.env('textoProdutoAdicionado'))
            .scrollIntoView({ duration: 200 })
            .should('be.visible');
    }
    
     /**
     * Verifica se o título e subtítulo da página da loja online correspondem aos valores esperados nas variáveis de ambiente.
     */
    validaTituloESubtituloPaginaLojaOnline = () => {
        cy.title()
          .should('eq', Cypress.env('tituloModalLoja'));
        cy.title()
          .should('eq', Cypress.env('homeTitle'));
    }

}

export default AdicionarProdutoPage;