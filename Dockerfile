FROM cypress/base:20.13.0

WORKDIR /app

RUN apt-get update && apt-get install -y \
    dbus-x11 \
    libgtk2.0-0 \
    libgtk-3-0 \
    libgbm-dev \
    libnotify-dev \
    libgconf-2-4 \
    libnss3 \
    libxss1 \
    libasound2 \
    libxtst6 \
    xauth \
    xvfb \
    && rm -rf /var/lib/apt/lists/*

RUN npm install cypress@13.9.0

COPY package*.json ./

RUN npm install

COPY . .

ENV DISPLAY=:99
ENV CYPRESS_INSTALL_BINARY=0
ENV NO_SANDBOX=true

EXPOSE 8080

CMD ["npm", "run", "test"]
